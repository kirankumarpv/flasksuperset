FROM alpine:3.5

# Update
RUN apk add --update python py-pip

# Install app dependencies
RUN pip install Flask

# Bundle app source
COPY simpleapp.py /src/simpleapp.py

EXPOSE  8088
CMD ["gunicron", "-w 4 -k gevent --timeout 120 -b  0.0.0.0:8088 --limit-request-line 0 --limit-request-field_size 0 --statsd-host localhost:8088 superset:app wsgi &", "-p 8088"]